import {
  faArrowCircleLeft,
  faEnvelope,
  faLocationDot,
  faPhone,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Card, CardContent, Divider, Grid, Typography } from '@mui/material';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';

const undefineLogo = 'https://cdn-icons-png.flaticon.com/512/508/508970.png';

export default function UniversityDetail() {
  const history = useNavigate();
  const university = JSON.parse(localStorage.getItem('university'));
  const faculties = university.faculty_kh.split(';');

  return (
    <>
      <Typography
        marginTop={4}
        marginLeft={8}
        marginBottom={1}
        style={{ alignItems: 'left', textAlign: 'left', cursor: 'pointer' }}
      >
        <FontAwesomeIcon
          icon={faArrowCircleLeft}
          color='rgb(37, 61, 107)'
          cursor='pointer'
          style={{ fontSize: '2.5rem' }}
          onClick={() => {
            history(-1);
          }}
        />
      </Typography>
      <Card sx={{ display: 'flex', margin: 'auto', mt: 2 }}>
        <Grid container>
          <Grid item xs={12} sm={4} padding={2}>
            <img
              src={!university.logoUrl ? undefineLogo : university.logoUrl}
              width={200}
              height={200}
            />
          </Grid>
          <Grid item xs={12} sm={8} sx={{ border: '1px solid #C1BAB9', paddingX: 4, paddingY: 2 }}>
            <CardContent sx={{ alignItems: 'left', textAlign: 'left' }}>
              <Typography variant='h5' color='#00256E' fontWeight={700} marginBottom={2}>
                {university.university_name}
              </Typography>
              <Typography>
                <FontAwesomeIcon color='rgb(37, 61, 107)' icon={faPhone} fontSize='1.2rem' />
                <Typography variant='p' color='#485D88' marginLeft={2} fontSize={15}>
                  {university.tel.replace(';', ',')}
                </Typography>
              </Typography>
              <br />
              <Typography>
                <FontAwesomeIcon color='rgb(37, 61, 107)' icon={faEnvelope} fontSize='1.2rem' />
                <Typography variant='p' color='#485D88' marginLeft={2} fontSize={15}>
                  {university.email}
                </Typography>
              </Typography>
              <br />
              <Typography>
                <FontAwesomeIcon color='rgb(37, 61, 107)' icon={faLocationDot} fontSize='1.2rem' />
                <Typography variant='p' color='#485D88' marginLeft={2} fontSize={15}>
                  {university.commune} {university.district} {university.province}
                </Typography>
              </Typography>
              <br />
              <Typography>
                <Typography variant='p' marginX={1} fontWeight={600} color='#204D67'>
                  Website:
                </Typography>
                <Typography variant='p' color='#485D88' marginLeft={2} fontSize={15}>
                  <a href={university.website}>{university.website}</a>
                </Typography>
              </Typography>
              <br />
              <Typography>
                <Typography variant='p' marginX={1} fontWeight={600} color='#204D67'>
                  Last updated:
                </Typography>
                <Typography variant='p' color='#485D88' marginLeft={2} fontSize={15}>
                  02/07/2018
                </Typography>
              </Typography>
              <Divider sx={{ mt: 2 }} />
              <div>
                <Typography variant='h5' color='#00256E' fontWeight={700} marginTop={2}>
                  Faculties
                </Typography>
                <ul style={{ padding: 8 }}>
                  {faculties.map((substring, index) => (
                    <li key={index} style={{ marginTop: 6 }}>
                      {substring}
                    </li>
                  ))}
                </ul>
              </div>
            </CardContent>
          </Grid>
        </Grid>
      </Card>
    </>
  );
}
