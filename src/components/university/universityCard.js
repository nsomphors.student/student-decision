import { Card, CardContent, Grid, Typography } from '@mui/material';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';

const undefineLogo = 'https://cdn-icons-png.flaticon.com/512/508/508970.png';

export default function UniversityCard({ university }) {
  const history = useNavigate();

  return (
    <Card
      sx={{ display: 'flex', maxWidth: 800, margin: 'auto', mt: 2 }}
      onClick={() => {
        localStorage.setItem('university', JSON.stringify(university));
        history(`/universities/${university.university_name}`);
      }}
    >
      <Grid container>
        <Grid item xs={12} sm={4} padding={2}>
          <img
            src={!university.logoUrl ? undefineLogo : university.logoUrl}
            width={180}
            height={180}
          />
        </Grid>
        <Grid item xs={12} sm={8}>
          <CardContent sx={{ padding: 2, alignItems: 'left', textAlign: 'left' }}>
            <Typography variant='h5' color='#00256E' fontWeight={700}>
              {university.university_name}
            </Typography>
            <Typography variant='div' display='flex'>
              <Typography variant='p' marginY={3} display='flex'>
                <span style={{ fontWeight: 600 }}>LOCATION:</span>
                {university.village} {university.commune} {university.district}{' '}
                {university.province}
              </Typography>
            </Typography>
            <Typography variant='div' display='flex'>
              <Typography variant='p' display='flex'>
                <span style={{ fontWeight: 600 }}>LINK Website:</span>{' '}
                <a href='https://www.puthisastra.edu.kh/'>https://www.puthisastra.edu.kh/</a>
              </Typography>
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </Card>
  );
}
