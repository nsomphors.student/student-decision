import React, { useState } from 'react';
import { Box, Grid, TextField, Button } from '@mui/material';
import { client } from '../../utils/axiosEvent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBarsProgress } from '@fortawesome/free-solid-svg-icons';

const ContactForm = () => {
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    subject: '',
    message: '',
  });
  const [message, setMessage] = useState();
  const [showAlert, setShowAlert] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setFormData({
      firstName: '',
      lastName: '',
      email: '',
      subject: '',
      message: '',
    });
    setLoading(true);
    client
      .post('/report', formData)
      .then((response) => {
        setMessage(response.data);
        // Show alert message
        setShowAlert(true);
        setTimeout(() => {
          setShowAlert(false);
        }, 3000);
      })
      .catch((e) => {
        confirm(e);
      });
    setLoading(false);

    // Now you can send the form data to your API using axios or any other method
  };

  return (
    <Box
      component='form'
      onSubmit={handleSubmit}
      sx={{ mt: 2, border: '1px solid #adacac', padding: 6, textAlign: 'left' }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            label='First Name'
            name='firstName'
            variant='standard'
            value={formData.firstName}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            variant='standard'
            label='Last Name'
            name='lastName'
            value={formData.lastName}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            variant='standard'
            label='Email'
            name='email'
            value={formData.email}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            variant='standard'
            label='Subject'
            name='subject'
            value={formData.subject}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            multiline
            variant='standard'
            rows={4}
            label='Message'
            name='message'
            value={formData.message}
            onChange={handleChange}
          />
        </Grid>
      </Grid>
      <Button
        endIcon={loading ?? <FontAwesomeIcon color='red' icon={faBarsProgress} />}
        sx={{ mt: 4, bgcolor: '#EF6D00' }}
        type='submit'
        variant='contained'
      >
        Send Message
      </Button>
      {showAlert && <div style={{ marginTop: 10, color: 'green' }}>{message}</div>}
    </Box>
  );
};

export default ContactForm;
