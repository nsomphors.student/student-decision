import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Divider,
  Grid,
  Stack,
  Typography,
} from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faPlayCircle } from '@fortawesome/free-solid-svg-icons';
import { client } from '../../utils/axiosEvent';
import React, { useState } from 'react';

function UniversityReasoning() {
  const [accordionData, setAccordionData] = useState([]);
  React.useEffect(() => {
    client.get('/fqa').then((response) => {
      setAccordionData(response.data);
    });
  });
  return (
    <>
      <Stack
        sx={{
          width: '100%',
          marginTop: '5rem',
          backgroundImage:
            'url(https://www.educationunlimited.com/blog/wp-content/uploads/2019/08/Webp.net-compress-image.jpg)',
          backgroundSize: 'cover',
          backgroundAttachment: 'fixed',
        }}
      >
        <Grid
          container
          height='100%'
          sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
        >
          <Grid item xs={6} height='100%'>
            <a href='https://www.youtube.com/watch?v=jFCFqjovH3s'>
              <FontAwesomeIcon color='white' fontSize='5rem' icon={faPlayCircle} />
            </a>
          </Grid>
          <Grid
            item
            xs={6}
            height='100%'
            sx={{
              backgroundImage:
                'url(https://educab.jollyany.co/images/ken-theimer-poe6q48b-5k-unsplash.jpg)',
              backgroundSize: 'cover',
            }}
          >
            <Stack
              height='100%'
              padding={8}
              sx={{ bgcolor: 'rgba(0, 37, 110, 0.9)', textAlign: 'left' }}
            >
              <Typography sx={{ display: 'flex', alignItems: 'center', color: '#87a2d5' }}>
                <Divider
                  sx={{
                    width: '7%',
                    height: '2px',
                    backgroundColor: '#87a2d5',
                  }}
                />
                <Typography variant='span' marginLeft={2}>
                  WHY CHOOSE UNIVERSITY
                </Typography>
              </Typography>
              <Typography variant='h3' fontWeight={800} color='white'>
                Why you like Our University
              </Typography>

              <Stack marginTop={4}>
                {accordionData.map((accordionItem) => (
                  <Accordion
                    key={accordionItem.id}
                    sx={{ marginBottom: 1, padding: 1, bgcolor: '#f6fafb', color: '#4b5981' }}
                  >
                    <AccordionSummary
                      expandIcon={<FontAwesomeIcon icon={faAngleDown} />}
                      aria-controls={`panel${accordionItem.id}-content`}
                      id={`panel${accordionItem.id}-header`}
                      // sx={{bgcolor:'#f6fafb'}}
                    >
                      <Typography>{accordionItem.summary}</Typography>
                    </AccordionSummary>
                    <Divider />

                    <AccordionDetails sx={{ paddingY: 3 }}>
                      <Typography>{accordionItem.content}</Typography>
                    </AccordionDetails>
                  </Accordion>
                ))}
              </Stack>
            </Stack>
          </Grid>
        </Grid>
      </Stack>
    </>
  );
}

export default UniversityReasoning;
