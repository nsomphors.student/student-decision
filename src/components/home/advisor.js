import React, { useState } from 'react';
import { Button, Container, Divider, Grid, Stack, Typography } from '@mui/material';
import AdvisorCard from '../advisor/advisorCard';
import { client } from '../../utils/axiosEvent';

const Advisor = () => {
  const [advisors, setAdvisors] = useState([]);
  React.useEffect(() => {
    client.get('/advisors').then((response) => {
      setAdvisors(response.data.slice(0, 4));
    });
  }, []);
  return (
    <Container maxWidth='lg' sx={{ marginY: 8, cursor: 'pointer' }}>
      <Typography sx={{ display: 'flex', alignItems: 'center', color: '#87a2d5' }}>
        <Divider
          sx={{
            width: '7%',
            height: '2px',
            backgroundColor: '#87a2d5',
          }}
        />
        <Typography variant='span' marginLeft={2}>
          THEY ARE ADVISORS
        </Typography>
      </Typography>
      <Typography variant='h3' fontWeight={800} color='#00256E' textAlign='left' paddingY={4}>
        Meet Our Advisors
      </Typography>
      <Stack direction='row' justifyContent='space-between' alignItems='center' marginBottom={4}>
        <Typography variant='p'>
          Nam in viverra mauris. Integer varius ullamcorper metus auctor porta. Pellentesque velit
          eros, malesuada sed lorem non, rhoncus blandit leo.
        </Typography>
        <Button
          sx={{
            paddingX: 2,
            bgcolor: 'rgb(247, 194, 33)',
            color: '#00256E',
            fontWeight: 600,
          }}
        >
          View More
        </Button>
      </Stack>

      <Grid container spacing={2}>
        {advisors.map((advisor, index) => (
          <Grid key={index} item xs={12} md={6} lg={3} display='flex' justifyContent='center'>
            <AdvisorCard advisor={advisor} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Advisor;
