import React from 'react';
import { CardContent, Typography, Container, Stack, Button, Grid } from '@mui/material';
import './../../styles/flippingCard.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarWeek, faUniversity, faUserFriends } from '@fortawesome/free-solid-svg-icons';
import { useNavigate } from 'react-router-dom';

const cards = [
  {
    id: 1,
    title: 'Advisors',
    description:
      "Successful businesses have many things in common, today we'll look at the big 'R' of recognitional advertising network may help.",
    icon: faUserFriends,
    route: '/advisors',
    color: '#5599FF',
  },
  {
    id: 2,
    title: 'Universities',
    description:
      "Successful businesses have many things in common, today we'll look at the big 'R' of recognitional advertising network may help.",
    icon: faUniversity,
    route: '/universities',
    color: '#FF5F69',
  },
  {
    id: 3,
    title: 'Events',
    description:
      "Successful businesses have many things in common, today we'll look at the big 'R' of recognitional advertising network may help.",
    icon: faCalendarWeek,
    route: '/event',
    color: '#11C0B2',
  },
];
function FlippingCard() {
  const history = useNavigate();

  const handleClickCard = React.useCallback((card) => {
    history(card?.route);
  }, []);
  return (
    <Container
      maxWidth='lg'
      sx={{ display: 'flex', justifyContent: 'space-between', marginTop: '-7rem' }}
    >
      <Grid container spacing={2}>
        {cards.map((card) => (
          <Grid item key={card.id} xs={12} sm={12} md={4}>
            <Stack
              className='container'
              sx={{
                height: '60vh',
                margin: '1rem',
                cursor: 'pointer',
                perspective: '1000px',
              }}
            >
              <div className='front' style={{ backgroundColor: card.color }}>
                <CardContent
                  sx={{
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: 'white',
                  }}
                >
                  <FontAwesomeIcon color='white' icon={card.icon} fontSize='3rem' />
                  <Typography
                    component='div'
                    align='center'
                    fontWeight={600}
                    fontSize={25}
                    marginY='1rem'
                  >
                    {card.title}
                  </Typography>
                  <Typography component='p'>{card.description}</Typography>
                </CardContent>
              </div>
              <div className='back' style={{ backgroundColor: card.color }}>
                <CardContent
                  sx={{
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: 'white',
                  }}
                >
                  <Typography
                    component='div'
                    align='center'
                    fontWeight={600}
                    fontSize={25}
                    marginY='1rem'
                  >
                    {card.title}
                  </Typography>
                  <Typography component='p'>{card.description}</Typography>
                  <Button
                    variant='outlined'
                    sx={{
                      color: '#00256e',
                      borderColor: '#00256e',
                      fontWeight: 'bold',
                      marginTop: '0.8rem',
                    }}
                    onClick={() => handleClickCard(card)}
                  >
                    Read More
                  </Button>
                </CardContent>
              </div>
            </Stack>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}

export default FlippingCard;
