import React, { useState } from 'react';
import { Box, Button, Paper, Stack, Typography } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';

const slides = [
  'https://educab.jollyany.co/images/alexis-brown--xv7k95vofa-unsplash.jpg',
  'https://educab.jollyany.co/images/lucas-law-pitoug-_vo0-unsplash.jpg',
];
const Slider = () => {
  const [currentIndex, setCurrentIndex] = useState(1);

  const handleNextSlide = () => {
    if (currentIndex === slides.length - 1) {
      setCurrentIndex(0);
    } else {
      setCurrentIndex(currentIndex + 1);
    }
  };

  const handlePrevSlide = () => {
    if (currentIndex === 0) {
      setCurrentIndex(slides.length - 1);
    } else {
      setCurrentIndex(currentIndex - 1);
    }
  };

  return (
    <Box>
      <Paper elevation={3}>
        <Box
          sx={{
            width: '100%',
            height: '110vh',
            position: 'relative',
            overflow: 'hidden',
          }}
        >
          {slides.map((slide, index) => (
            <Box
              key={index}
              sx={{
                width: '100%',
                height: '110vh',
                backgroundImage: `url(${slides[currentIndex]})`,
                backgroundSize: 'cover',
                position: 'absolute',
                left: index * 100 + '%',
                transform: `translateX(-${currentIndex * 100}%)`,
                transition: 'transform 0.5s',
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <Button onClick={handlePrevSlide} fontWeight='bold'>
                <FontAwesomeIcon color='white' icon={faAngleLeft} fontSize='1.7rem' opacity='0.8' />
              </Button>
              <Stack
                sx={{
                  color: 'white',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Typography fontSize={26} fontWeight={600}>
                  Best University for Education
                </Typography>
                <Typography fontSize={18} marginTop={1}>
                  You might remember the Dell computer commercials in which a youth reports this
                  exciting news to his friends.
                </Typography>
                <Button
                  sx={{
                    width: '20%',
                    bgcolor: 'rgb(247, 194, 33)',
                    color: '#00256E',
                    fontWeight: 600,
                    marginTop: '5rem',
                  }}
                >
                  View More
                </Button>
              </Stack>
              <Button onClick={handleNextSlide}>
                <FontAwesomeIcon
                  color='white'
                  icon={faAngleRight}
                  fontSize='1.7rem'
                  fontWeight='bold'
                  opacity='0.8'
                />
              </Button>
            </Box>
          ))}
        </Box>
      </Paper>
    </Box>
  );
};

export default Slider;
