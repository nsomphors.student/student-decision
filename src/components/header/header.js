import React from 'react';
import { Container, Stack, Typography } from '@mui/material';
import { useNavigate } from 'react-router';

export const Header = ({ title, ...props }) => {
  const history = useNavigate();
  return (
    <>
      <Stack
        width='100%'
        height='60vh'
        style={{
          backgroundImage:
            'url(https://educab.jollyany.co/images/ben-duchac-96DW4Pow3qI-unsplash.jpg)',
          backgroundSize: 'cover',
        }}
        direction='row'
        alignItems='center'
      >
        <Container
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            color: '#ffffff',
          }}
        >
          <Typography variant='h3' fontWeight={700} {...props?.titleStyle}>
            {title}
          </Typography>
          <Stack direction='row' spacing={3}>
            <Typography variant='p'>You are Here: </Typography>
            <Typography
              variant='a'
              style={{ cursor: 'pointer' }}
              onClick={() => {
                history('/home');
              }}
            >
              Home
            </Typography>
            <Typography variant='p'>/</Typography>
            <Typography variant='p' color='#666666'>
              {title}
            </Typography>
          </Stack>
        </Container>
      </Stack>
    </>
  );
};
