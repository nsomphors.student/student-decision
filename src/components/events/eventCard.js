import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Box, Button, CardActionArea } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar, faEnvelope, faPhone, faUser } from '@fortawesome/free-solid-svg-icons';

const getFormatUniversityName = (title) => title?.match(/\(([^)]+)\)/)?.[1];

const EventCard = ({ event, onClick, ...props }) => {
  const handleClick = React.useCallback(() => {
    onClick(event);
  }, [event, onClick]);

  return (
    <Card
      sx={{
        overflow: 'hidden',
        boxShadow: '0 4px 8px 0 rgba(3, 10, 105,0.4)',
        pb: '20px',
        bgcolor: event?.color,
        transition: 'transform 0.3s ease',
        '&:hover': {
          transform: 'scale(1.05)',
        },
        width: '50%',
      }}
      elevation={3}
      {...props}
    >
      <Typography
        maxWidth='60px'
        fontSize='18px'
        fontWeight='600'
        mt='16px'
        mx='10px'
        color='white'
        position='absolute'
      >
        {getFormatUniversityName(event?.title)}
      </Typography>
      <CardActionArea
        sx={{
          height: '100%',
          pb: '16px',
          bgcolor: 'white',
          overflow: 'hidden',
          borderTopLeftRadius: '160px',
        }}
      >
        <CardMedia
          component='img'
          height='50%'
          image={event?.image}
          sx={{ borderBottomLeftRadius: '15%', borderBottomRightRadius: '15%' }}
          alt='green iguana'
        />
        <CardContent
          sx={{
            height: '60%',
            py: '32px',
          }}
        >
          <Button
            onClick={handleClick}
            variant='contained'
            sx={{
              position: 'absolute',
              top: '48%',
              transform: 'translate(-50%, -50%)',
              '&:hover': {
                bgcolor: 'teal',
              },
              alignSelf: 'center',
              background: event?.color,
              fontSize: {
                sm: '12px',
                md: '12px',
                lg: '16px',
              },
              width: {
                sm: '70%',
                md: '50%',
                lg: '40%',
              },
            }}
          >
            Join Event
          </Button>
          <Typography
            gutterBottom
            component='div'
            fontSize={{
              sm: '16px',
              md: '20px',
              lg: '20px',
            }}
            fontFamily='revert-layer'
            fontWeight='600'
            color={event?.color}
          >
            {event?.title}
          </Typography>
          <Typography fontSize='16px' fontFamily='sans' color='text.secondary'>
            {event?.description}
          </Typography>
          <Box width='100%' mb='32px' mt="16px">
            <Box display='flex' justifyContent='flex-start' alignItems='center'>
              <FontAwesomeIcon
                icon={faEnvelope}
                style={{ fontSize: '0.9rem', padding: '4px', color: 'gray' }}
              />
              <Typography fontSize='16px' fontFamily='sans' color='text.secondary' ml='12px'>
                {event?.address}
              </Typography>
            </Box>
            <Box display='flex' justifyContent='flex-start' alignItems='center'>
              <FontAwesomeIcon
                icon={faCalendar}
                style={{ fontSize: '0.9rem', padding: '4px', color: 'gray' }}
                mr='12px'
              />
              <Typography fontSize='16px' fontFamily='sans' color='text.secondary' ml='12px'>
                {event?.date}
              </Typography>
            </Box>
            <Box display='flex' justifyContent='flex-start' alignItems='center'>
              <FontAwesomeIcon
                icon={faUser}
                style={{ fontSize: '0.9rem', padding: '4px', color: 'gray' }}
              />
              <Typography fontSize='16px' fontFamily='sans' color='text.secondary' ml='12px'>
                {event?.totalJoin}
              </Typography>
            </Box>
            <Box display='flex' justifyContent='flex-start' alignItems='center'>
              <FontAwesomeIcon
                icon={faPhone}
                style={{ fontSize: '0.9rem', padding: '4px', color: 'gray' }}
              />
              <Typography fontSize='16px' fontFamily='sans' color='text.secondary' ml='12px'>
                {event?.tell}
              </Typography>
            </Box>
          </Box>
        </CardContent>
      </CardActionArea>
      <Box width='100%' display='flex' alignItems='center' px='12px'>
        <FontAwesomeIcon
          icon={faEnvelope}
          style={{ fontSize: '0.9rem', padding: '4px', color: '#f2f2f2' }}
        />
        <Typography
          fontSize='12px'
          fontWeight='400'
          mx='10px'
          color='#f2f2f2'
          textAlign='center'
          ml='12px'
        >
          {event?.email}
        </Typography>
      </Box>
    </Card>
  );
};

export default EventCard;
