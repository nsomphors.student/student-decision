import React from 'react';
import {
  Button,
  Modal,
  Box,
  Typography,
  TextField,
  Snackbar,
  Alert,
  CircularProgress,
} from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';

const isValidEmail = (email) => {
  const emailRegex = /\S+@\S+\.\S+/;
  return emailRegex.test(email);
};

const isValidPhoneNumber = (phone) => {
  const phoneRegex = /^\d{10}$/;
  return phoneRegex.test(phone);
};

const EventFormModal = ({ eventData, openModal, onOpenModal }) => {
  const [username, setUsername] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [showAlert, setShowAlert] = React.useState(false);
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const [error, setError] = React.useState({
    email: false,
    phone: false,
    username: false,
  });

  const handleCloseModal = React.useCallback(() => {
    onOpenModal();
    setError({ email: false, phone: false, date: false, username: false });
    setEmail('');
    setUsername('');
    setPhoneNumber('');
  }, [onOpenModal]);

  const handleEmailChange = React.useCallback(
    (event) => {
      setEmail(event?.target?.value);
      setError({ ...error, email: !isValidEmail(event?.target?.value) });
    },
    [error],
  );

  const handlePhoneChange = React.useCallback(
    (event) => {
      setPhoneNumber(event?.target?.value);
      setError({ ...error, phone: !isValidPhoneNumber(event?.target?.value) });
    },
    [error],
  );

  const handleChange = React.useCallback((event) => {
    setUsername(event?.target?.value);
  }, []);

  const handleSubmit = React.useCallback(
    (e) => {
      e.preventDefault();
      if (
        isValidEmail(email) &&
        isValidPhoneNumber(phoneNumber) &&
        username !== ''
      ) {
        setIsSubmitting(true);
        setTimeout(() => {
          setShowAlert(true);
          setIsSubmitting(false);
          handleCloseModal();
        }, 3000);
      } else {
        const validateForm = { email: false, phone: false, username: false };
        if (username === '') {
          validateForm.username = true;
        }
        if (!isValidEmail(email)) {
         validateForm.email = true;
        }
        if (!isValidPhoneNumber(phoneNumber)) {
            validateForm.phone = true;
        }
        setError(validateForm);
      }
    },
    [handleCloseModal, email, phoneNumber, username, error],
  );

  React.useEffect(() => {
    if (showAlert) {
      setTimeout(() => {
        setShowAlert(false);
      }, 3000);
    }
  }, [showAlert]);

  return (
    <>
      <Snackbar
        open={showAlert}
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert severity='success' sx={{ width: '100%' }}>
          You have register with our event successfully.
        </Alert>
      </Snackbar>
      <Modal
        open={openModal}
        aria-labelledby='simple-modal-title'
        aria-describedby='simple-modal-description'
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: {
              xs: '80%',
              sm: '80%',
              md: '60%',
              lg: '50%',
            },
            borderRadius: '20px',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            alignItems: 'center',
            textAlign: 'center',
            filter: isSubmitting ? 'blur(0.5px)' : 'none',
            pointerEvents: isSubmitting ? 'none' : null,
          }}
        >
          <CancelIcon
            onClick={handleCloseModal}
            sx={{
              position: 'absolute',
              top: 10,
              right: 8,
              cursor: 'pointer',
              color: 'darkRed',
              width: '25px',
              height: '25px',
            }}
          />
          <Typography
            component='h2'
            sx={{
              alignSelf: 'center',
              color: eventData?.color,
              fontSize: '20px',
              fontWeight: '600',
              textTransform: 'uppercase',
            }}
          >
            Register to join Event
          </Typography>
          <TextField
            id='email'
            label='Email'
            variant='outlined'
            value={email}
            onChange={handleEmailChange}
            error={error?.email}
            helperText={error?.email ? 'Please enter a valid email' : ''}
            sx={{ mt: 2, width: '100%' }}
            required
          />
          <TextField
            id='phone'
            label='Phone Number'
            variant='outlined'
            value={phoneNumber}
            onChange={handlePhoneChange}
            error={error?.phone}
            helperText={error?.phone ? 'Please enter a valid phone number (10 digits)' : ''}
            sx={{ mt: 2, width: '100%' }}
            required
          />
          <TextField
            id='input-name'
            label='username'
            variant='outlined'
            value={username}
            onChange={handleChange}
            sx={{ mt: 2, width: '100%' }}
            error={error?.username}
            helperText={error?.username ? 'Please fill this field' : ''}
            required
          />
          <Button
            variant='contained'
            onClick={(e) => handleSubmit(e)}
            sx={{
              marginTop: '30px',
              width: '80%',
              background: eventData?.color,
              height: '50px',
              '&:hover': {
                background: eventData?.color,
              },
            }}
          >
            {isSubmitting ? <CircularProgress size={24} style={{ color: 'white' }} /> : 'Submit'}
          </Button>
        </Box>
      </Modal>
    </>
  );
};

export default EventFormModal;
