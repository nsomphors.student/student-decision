import React from 'react';
import { Toolbar, Button, Stack, Box } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faEnvelope, faLocationDot } from '@fortawesome/free-solid-svg-icons';
import {
  faFacebook,
  faInstagram,
  faPinterest,
  faTelegram,
} from '@fortawesome/free-brands-svg-icons';
import { useNavigate } from 'react-router-dom';

export default function Navigation() {
  const history = useNavigate();
  return (
    <>
      <Box
        display='flex'
        justifyContent='space-between'
        color='#ffffff'
        padding={2}
        sx={{ paddingX: 8 }}
        bgcolor='rgb(37, 61, 107)'
      >
        <Stack direction='row' spacing={2}>
          <FontAwesomeIcon icon={faFacebook} style={{ fontSize: '1.5rem' }} />
          <FontAwesomeIcon icon={faPinterest} style={{ fontSize: '1.5rem' }} />
          <FontAwesomeIcon icon={faInstagram} style={{ fontSize: '1.5rem' }} />
          <FontAwesomeIcon icon={faTelegram} style={{ fontSize: '1.5rem' }} />
        </Stack>
        <Stack direction='row' spacing={2}>
          <Stack direction='row' spacing={2} fontSize='0.7rem'>
            <div>
              <FontAwesomeIcon
                icon={faLocationDot}
                style={{ fontSize: '0.9rem', paddingRight: 4 }}
              />
              <span> 1826 Locust Street, Bainbridge </span>
            </div>
            <div>
              <FontAwesomeIcon icon={faEnvelope} style={{ fontSize: '0.9rem', paddingRight: 4 }} />
              <span> ngoun.somphors@gmail.com</span>
            </div>
          </Stack>
          <a href=''>
            <img
              title='Hebrew'
              src='https://educab.jollyany.co/media/mod_languages/images/en_gb.gif'
              alt='Hebrew'
            />
          </a>
          <a href=''>
            <img
              title='Hebrew'
              src='https://cdn.countryflags.com/thumbs/cambodia/flag-wave-250.png'
              alt='Hebrew'
              width='20'
              height='12'
            />
          </a>
        </Stack>
      </Box>
      <Toolbar
        sx={{
          backgroundColor: 'white',
          color: '#00256e',
          display: 'flex',
          justifyContent: 'space-between',
          padding: '0 50px !important',
          position: 'sticky',
          top: 0,
          zIndex: 1000,
        }}
      >
        <img
          title='Hebrew'
          src='https://educab.jollyany.co/images/tz_educab/logo.png'
          alt='Hebrew'
        />
        <Stack direction='row'>
          <Button
            color='inherit'
            fontWeight={800}
            onClick={() => {
              history('/');
            }}
          >
            Home
          </Button>
          <Button
            color='inherit'
            onClick={() => {
              history('/universities');
            }}
          >
            Universities
          </Button>
          <Button
            color='inherit'
            onClick={() => {
              console.log('Event page');
              history('/event');
            }}
          >
            Events
          </Button>
          <Button
            color='inherit'
            onClick={() => {
              history('/advisors');
            }}
          >
            Advisors
          </Button>

          <Button
            color='inherit'
            onClick={() => {
              history('/contact-us');
            }}
          >
            Contact Us
          </Button>
          <FontAwesomeIcon icon={faBars} style={{ fontSize: '1.5rem', marginLeft: '10px' }} />
        </Stack>
      </Toolbar>
    </>
  );
}
