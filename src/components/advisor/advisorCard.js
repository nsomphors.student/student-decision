import React from 'react';
import { CardContent, CardMedia, Typography, Stack } from '@mui/material';
import { useNavigate } from 'react-router-dom';

const AdvisorCard = ({ advisor }) => {
  const history = useNavigate();

  return (
    <Stack
      sx={{ maxWidth: 320 }}
      boxShadow={0}
      onClick={() => {
        localStorage.setItem('advisorDetail', JSON.stringify(advisor));
        history(`/advisors/${advisor.name}`);
      }}
    >
      <CardMedia
        component='img'
        height='450'
        image={advisor.photo}
        alt='Alisa Katherine'
        sx={{
          '&:hover': {
            transform: 'scale(1.1)',
            transition: 'transform 0.3s ease-in-out',
          },
        }}
      />
      <CardContent
        sx={{ display: 'flex', justifyContent: 'center', padding: 0, mt: -6.5, zIndex: 10 }}
      >
        <Stack width='80%' bgcolor='#ffffff' boxShadow={1} padding={3}>
          <Typography gutterBottom variant='h6' component='div' color='#485D88' fontWeight={700}>
            {advisor.name}
          </Typography>
          <Typography variant='body2' color='text.secondary' fontWeight={600}>
            {advisor.position}
          </Typography>
        </Stack>
      </CardContent>
    </Stack>
  );
};

export default AdvisorCard;
