import React from 'react';
import { CardMedia, Container, Grid, Stack, Typography } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faInstagram,
  faPinterest,
  faSquareFacebook,
  faTelegram,
} from '@fortawesome/free-brands-svg-icons';
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { useNavigate, useParams } from 'react-router-dom';

const AdvisorDetail = () => {
  const { advisorName } = useParams();
  const history = useNavigate();
  const advisorDetail = JSON.parse(localStorage.getItem('advisorDetail'));
  return (
    <>
      <Container>
        <Typography
          marginTop={4}
          marginBottom={1}
          style={{ alignItems: 'left', textAlign: 'left', cursor: 'pointer' }}
        >
          <FontAwesomeIcon
            icon={faArrowCircleLeft}
            color='rgb(37, 61, 107)'
            cursor='pointer'
            style={{ fontSize: '2.5rem' }}
            onClick={() => {
              history(-1);
            }}
          />
        </Typography>
        <Grid container spacing={4} boxShadow='0px 4px 8px rgba(0, 0, 0, 0.1)'>
          <Grid item xs={4} style={{ alignItems: 'left', textAlign: 'left' }} padding={4}>
            <CardMedia component='img' image={advisorDetail.photo} alt='Alisa Katherine' />
          </Grid>
          <Grid item xs={8} paddingX={4} style={{ alignItems: 'left', textAlign: 'left' }}>
            <Typography variant='h4' color='rgb(37, 61, 107)' fontWeight={700}>
              {advisorName.replace('-', ' ')}
            </Typography>
            <Typography variant='h6' sx={{ marginBottom: 2 }} color='#52DD36'>
              Professor of Physics
            </Typography>
            <Typography variant='p' color='#485D88' style={{ width: '50%' }}>
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
              ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
              quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on
              it squid single-origin coffee nulla assumenda shoreditch et.
            </Typography>
            <Typography variant='h5' fontWeight={800} marginTop={2}>
              Contact
            </Typography>
            <Stack marginTop={2}>
              <Typography variant='p' color='#485D88' lineHeight={2}>
                <Typography variant='span' fontWeight={800} fontSize={18}>
                  Phone Number:{' '}
                </Typography>{' '}
                {advisorDetail.phoneNumber}
              </Typography>
              <Typography variant='p' color='#485D88' lineHeight={2}>
                <Typography variant='span' fontWeight={800} fontSize={18}>
                  Email:{' '}
                </Typography>{' '}
                {advisorDetail.email}
              </Typography>
              <Stack direction='row' spacing={4} marginTop={4}>
                <a href='https://www.facebook.com/' target='_blank' rel='noopener noreferrer'>
                  <FontAwesomeIcon
                    icon={faSquareFacebook}
                    style={{ fontSize: '2.5rem' }}
                    color='#316FF6'
                  />
                </a>
                <a href='https://www.pinterest.com/' target='_blank' rel='noopener noreferrer'>
                  <FontAwesomeIcon
                    icon={faPinterest}
                    style={{ fontSize: '2.5rem' }}
                    color='#E60023'
                  />
                </a>
                <a href='https://web.telegram.org/a/' target='_blank' rel='noopener noreferrer'>
                  <FontAwesomeIcon
                    icon={faTelegram}
                    style={{ fontSize: '2.5rem' }}
                    color='#0088cc'
                  />
                </a>
                <a href='https://www.instagram.com/' target='_blank' rel='noopener noreferrer'>
                  <FontAwesomeIcon
                    icon={faInstagram}
                    color='#E1306C'
                    style={{ fontSize: '2.5rem' }}
                  />
                </a>
              </Stack>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default AdvisorDetail;
