import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faLocationDot } from '@fortawesome/free-solid-svg-icons';
import { faTelegram } from '@fortawesome/free-brands-svg-icons';
import { Grid } from '@mui/material';
import { useNavigate } from 'react-router';

const Footer = () => {
  const footerStyle = {
    textAlign: 'center',
    width: '100%',
    color: '#ffffff',
    backgroundColor: '#00256e',
  };

  const addressStyle = {
    textAlign: 'left',
    // backgroundColor: '#00256e',
    color: '#ffffff',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'start',
    justifyContent: 'space-between',
    padding: '50px 150px',
  };

  const history = useNavigate();

  return (
    <footer style={footerStyle}>
      <Grid container rowSpacing={1}>
        <Grid item xs={6}>
          <div className='footer-address' style={addressStyle}>
            <h2>
              <img
                title='Hebrew'
                src='https://educab.jollyany.co/images/tz_educab/logo.png'
                alt='Hebrew'
              />
            </h2>
            <div style={{ padding: '10px' }}>
              <FontAwesomeIcon
                icon={faEnvelope}
                style={{ fontSize: '0.9rem', paddingRight: '4px' }}
              />
              <span> 1826 Locust Street, Bainbridge </span>
            </div>
            <div style={{ padding: '10px' }}>
              <FontAwesomeIcon
                icon={faTelegram}
                style={{ fontSize: '0.9rem', paddingRight: '4px' }}
              />
              <span> 0973345299 </span>
              <br></br>
              <span style={{ padding: '23px' }}> 0973345299 </span>
            </div>
            <div style={{ padding: '10px', paddingBottom: '73px' }}>
              <FontAwesomeIcon
                icon={faLocationDot}
                style={{ fontSize: '0.9rem', paddingRight: '4px' }}
              />
              <span> ngoun.somphors@gmail.com</span>
            </div>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className='footer-nav' style={addressStyle}>
            <div style={{ padding: '10px', cursor: 'pointer' }}>
              <h2> Navigation </h2>
            </div>
            <a style={{ padding: '10px', cursor: 'pointer' }} onClick={() => history('/')}>
              Home
            </a>
            <a
              style={{ padding: '10px', cursor: 'pointer' }}
              onClick={() => history('/universities')}
            >
              Universities
            </a>
            <a style={{ padding: '10px', cursor: 'pointer' }} onClick={() => history('/event')}>
              Events
            </a>
            <a style={{ padding: '10px', cursor: 'pointer' }} onClick={() => history('/advisors')}>
              Advisors
            </a>
            <a
              style={{ padding: '10px', cursor: 'pointer' }}
              onClick={() => history('/contact-us')}
            >
              Contact Us
            </a>
          </div>
        </Grid>
      </Grid>
    </footer>
  );
};
export default Footer;
