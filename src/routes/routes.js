import React, { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';

import Home from '../views/home';
import ContactUs from '../views/contactUs';
import EventPage from '../views/event';
import AdvisorDetail from '../components/advisor/advisorDetail';
import Advisors from '../views/advisors';
import Universities from '../views/universities';
import UniversityDetail from '../components/university/universityDetail';
import EventDetail from '../views/eventDetail';

const Routers = () => {
  return (
    <Suspense fallback={<div />}>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/home' element={<Home />} />
        <Route path='/contact-us' element={<ContactUs />} />
        <Route path='/event' element={<EventPage />} />
        <Route path='/event/event-detail' element={<EventDetail />} />
        <Route path='/advisors/:advisorName' element={<AdvisorDetail />} />
        <Route path='/advisors' element={<Advisors />} />
        <Route path='/universities' element={<Universities />} />
        <Route path='/universities/:universityName' element={<UniversityDetail />} />
      </Routes>
    </Suspense>
  );
};

export default Routers;
