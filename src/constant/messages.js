export const eventDescription = {
  firstDescription: `Embark on a journey of educational exploration and informed decision-making with our personalized student-advisor discussions on university choices. Our sessions are tailored to guide students through the intricate maze of higher education options, providing invaluable insights and clarity.`,
  secondDescription: `In these engaging discussions, students have the opportunity to articulate their academic aspirations, passions, and ambitions. Our experienced advisors lend a supportive ear, understanding each student's unique strengths and interests to offer tailored guidance.`,
  thirstDescription: `Exploring the vast landscape of universities, our advisors delve into a comprehensive array of topics. From discussing academic preferences and potential majors to outlining admission requirements and deadlines, we cover every aspect vital to your university selection journey.`,
  fourthDescription: `The types of public events these universities host can vary widely, including academic conferences, seminars on specific topics, workshops, cultural festivals, and guest lectures by experts from various fields.`,
  fiveDescription: `Join us for an engaging opportunity where students can connect directly with seasoned advisors to explore, discuss, and seek advice on various academic, career, and personal development topics. Our forum is a hub for fostering meaningful conversations, sharing experiences, and gaining valuable insights to navigate the complexities of university life and beyond.`,
  descriptionList: [
    'Interactive sessions covering a spectrum of subjects from academic planning to career pathways.',
    'A supportive environment encouraging open dialogue, questions, and diverse perspectives.',
    'Access to expert guidance and mentorship from our esteemed panel of advisors.',
    'Networking opportunities to connect with peers and expand your circle.',
  ],
};