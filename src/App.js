import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';

import Routers from './routes/routes';
import Navigation from './components/menu/navigation';
import Footer from './components/footer/footers';

function App() {
  return (
    <div className='App'>
      <Router>
        <Navigation />
        <Routers />
        <Footer />
      </Router>
    </div>
  );
}

export default App;
