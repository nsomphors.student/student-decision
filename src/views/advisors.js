import React, { useState } from 'react';
import { Header } from '../components/header/header';
import { Container, Divider, Grid, Pagination, TextField, Typography } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import AdvisorCard from '../components/advisor/advisorCard';
import { client } from '../utils/axiosEvent';
// import { getAdvisorInfo } from '../utils/advisorUtil';

function Advisors() {
  // const advisors = getAdvisorInfo();
  const [advisors, setAdvisors] = useState([]);
  React.useEffect(() => {
    client.get('/advisors').then((response) => {
      setAdvisors(response.data);
    });
  }, []);
  const [searchQuery, setSearchQuery] = useState('');
  const filteredAdvisors = advisors.filter(
    (advisor) =>
      advisor.name.toLowerCase().includes(searchQuery.toLowerCase()) ||
      advisor.position.toLowerCase().includes(searchQuery.toLowerCase()),
  );
  const itemsPerPage = 8;
  const totalPages = Math.ceil(filteredAdvisors.length / itemsPerPage);

  const [currentPage, setCurrentPage] = useState(1);

  const handlePageChange = (event, newPage) => {
    setCurrentPage(newPage);
  };

  // Calculate the range of items to display on the current page
  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const displayedAdvisors = filteredAdvisors.slice(startIndex, endIndex);
  return (
    <>
      <Header title='Advisor' />
      <Typography
        maxWidth='lg'
        sx={{
          display: 'flex',
          alignItems: 'center',
          color: '#87a2d5',
          padding: 4,
          margin: 'auto',
        }}
      >
        <Divider
          sx={{
            width: '7%',
            height: '2px',
            backgroundColor: '#87a2d5',
          }}
        />
        <Typography variant='span' marginLeft={2}>
          THEY ARE ADVISORS
        </Typography>
      </Typography>
      <Container maxWidth='lg' sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography variant='h4' fontWeight={800} color='#00256E' textAlign='left'>
          Meet Our Advisors
        </Typography>
        <TextField
          id='input-with-icon-textfield'
          label=''
          InputProps={{
            startAdornment: (
              <FontAwesomeIcon icon={faSearch} color='gray' style={{ fontSize: '1.5rem' }} />
            ),
          }}
          variant='standard'
          onChange={(e) => setSearchQuery(e.target.value)}
        />
      </Container>
      <Container sx={{ mt: 2 }}>
        <Divider />
      </Container>
      <Container maxWidth='lg' sx={{ marginTop: 8, cursor: 'pointer' }}>
        <Grid container spacing={2}>
          {displayedAdvisors.map((advisor, index) => (
            <Grid key={index} item xs={12} md={6} lg={3} display='flex' justifyContent='center'>
              <AdvisorCard advisor={advisor} />
            </Grid>
          ))}
        </Grid>
      </Container>
      <Container
        maxWidth='lg'
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginY: 4,
        }}
      >
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
          sx={{ margin: 'auto', mt: 3 }}
        />
      </Container>
    </>
  );
}

export default Advisors;
