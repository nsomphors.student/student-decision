import React, { useState } from 'react';
import { Header } from '../components/header/header';
import { Container, Divider, Pagination, TextField, Typography } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
// import universitiesList from '../assets/universitiesList.json';
import UniversityCard from '../components/university/universityCard';
import { client } from '../utils/axiosEvent';

function Universities() {
  const [searchQuery, setSearchQuery] = useState('');
  const [university, setUniversity] = useState([]);
  React.useEffect(() => {
    client.get('/universities').then((response) => {
      setUniversity(response.data);
    });
  }, []);
  const filteredUniversity = university.filter((advisor) =>
    advisor.university_name.toLowerCase().includes(searchQuery.toLowerCase()),
  );
  const itemsPerPage = 8;
  const totalPages = Math.ceil(filteredUniversity.length / itemsPerPage);

  const [currentPage, setCurrentPage] = useState(1);

  const handlePageChange = (event, newPage) => {
    setCurrentPage(newPage);
  };

  // Calculate the range of items to display on the current page
  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const displayedUniversity = filteredUniversity.slice(startIndex, endIndex);
  return (
    <>
      <Header title='Universities' />
      <Typography
        maxWidth='lg'
        sx={{
          display: 'flex',
          alignItems: 'center',
          color: '#87a2d5',
          padding: 4,
          margin: 'auto',
        }}
      >
        <Divider
          sx={{
            width: '7%',
            height: '2px',
            backgroundColor: '#87a2d5',
          }}
        />
        <Typography variant='span' marginLeft={2}>
          WHY UNIVERSITY
        </Typography>
      </Typography>
      <Container maxWidth='lg' sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography variant='h4' fontWeight={800} color='#00256E' textAlign='left'>
          Find Your University
        </Typography>
        <TextField
          id='input-with-icon-textfield'
          label=''
          InputProps={{
            startAdornment: (
              <FontAwesomeIcon icon={faSearch} color='gray' style={{ fontSize: '1.5rem' }} />
            ),
          }}
          variant='standard'
          onChange={(e) => setSearchQuery(e.target.value)}
        />
      </Container>
      <Container sx={{ mt: 2 }}>
        <Divider maxWidth='lg' />
      </Container>
      <Container maxWidth='lg' sx={{ marginTop: 8, cursor: 'pointer' }}>
        {displayedUniversity.map((university, index) => (
          <React.Fragment key={index}>
            <UniversityCard university={university} />
          </React.Fragment>
        ))}
      </Container>
      <Container
        maxWidth='lg'
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginY: 4,
        }}
      >
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
          sx={{ margin: 'auto', mt: 3 }}
        />
      </Container>
    </>
  );
}

export default Universities;
