import { Box, Button, CardMedia, Container, Stack, Typography } from '@mui/material';
import React from 'react';
import { useLocation } from 'react-router-dom';
import { Header } from '../components/header/header';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar, faEnvelope, faPhone, faUser } from '@fortawesome/free-solid-svg-icons';
import EventFormModal from '../components/events/form';

const getFormatSchoolName = (schoolName) => schoolName?.split('(')[0].trim();

const EventDetail = () => {
  const location = useLocation();
  const [eventDetail, setEventDetail] = React.useState(null);
  const [openModal, setOpenModal] = React.useState(false);

  const handleOpenModal = React.useCallback(() => {
    setOpenModal(!openModal);
  }, [openModal]);

  React.useEffect(() => {
    setEventDetail(location?.state);
  }, [location?.state]);

  return (
    <Box bgcolor='#f5f5f5'>
      <Header
        title={getFormatSchoolName(eventDetail?.title)}
        height='40vh'
        titleStyle={{ sx: { fontSize: '25px', lineHeight: '24px', letterSpace: '-0.24' } }}
      />
      <Container sx={{ mt: '10vh' }}>
        {eventDetail?.detail?.map((item) => {
          if (item?.type === 'Map') {
            return (
              <Stack sx={{ width: '100%', height: '50vh', my: '32px', mb: '60px' }} key={item?.id}>
                <iframe
                  title='Google Map'
                  width='100%'
                  height='100%'
                  // frameBorder="0"
                  style={{ border: 0 }}
                  src={item?.img}
                  allowFullScreen
                ></iframe>
                <Typography
                  fontSize='16px'
                  fontWeight={500}
                  color='dark'
                  textAlign='start'
                  py='5vh'
                >
                  {eventDetail?.description}
                </Typography>
              </Stack>
            );
          } else {
            return (
              <Box key={item?.id}>
                <CardMedia component='img' height='100%' image={item?.img} alt='green iguana' />
                <Typography
                  fontSize='20px'
                  fontFamily='sans'
                  color='teal'
                  ml='12px'
                  fontWeight='600'
                  my='32px'
                >
                  {item?.title}
                </Typography>
                {item?.id === 1 && (
                  <Box
                    width='100%'
                    mb='32px'
                    mt='16px'
                    sx={{
                      boxShadow: 2,
                      py: '16px',
                      px: '32px',
                    }}
                  >
                    <Box display='flex' justifyContent='flex-start' alignItems='center'>
                      <FontAwesomeIcon
                        icon={faEnvelope}
                        style={{ fontSize: '0.9rem', padding: '4px', color: 'gray' }}
                      />
                      <Typography
                        fontSize='16px'
                        fontFamily='sans'
                        color='text.secondary'
                        ml='12px'
                      >
                        {eventDetail?.address}
                      </Typography>
                    </Box>
                    <Box display='flex' justifyContent='flex-start' alignItems='center'>
                      <FontAwesomeIcon
                        icon={faCalendar}
                        style={{ fontSize: '0.9rem', padding: '4px', color: 'gray' }}
                        mr='12px'
                      />
                      <Typography
                        fontSize='16px'
                        fontFamily='sans'
                        color='text.secondary'
                        ml='12px'
                      >
                        {eventDetail?.date}
                      </Typography>
                    </Box>
                    <Box display='flex' justifyContent='flex-start' alignItems='center'>
                      <FontAwesomeIcon
                        icon={faUser}
                        style={{ fontSize: '0.9rem', padding: '4px', color: 'gray' }}
                      />
                      <Typography
                        fontSize='16px'
                        fontFamily='sans'
                        color='text.secondary'
                        ml='12px'
                      >
                        {eventDetail?.totalJoin}
                      </Typography>
                    </Box>
                    <Box display='flex' justifyContent='flex-start' alignItems='center'>
                      <FontAwesomeIcon
                        icon={faPhone}
                        style={{ fontSize: '0.9rem', padding: '4px', color: 'gray' }}
                      />
                      <Typography
                        fontSize='16px'
                        fontFamily='sans'
                        color='text.secondary'
                        ml='12px'
                      >
                        {eventDetail?.tell}
                      </Typography>
                    </Box>
                    <Button
                      onClick={handleOpenModal}
                      variant='contained'
                      sx={{
                        '&:hover': {
                          bgcolor: 'teal',
                        },
                        background: eventDetail?.color,
                        fontSize: {
                          sm: '12px',
                          md: '12px',
                          lg: '16px',
                        },
                        mt: '20px',
                        left: '0',
                      }}
                    >
                      Register To Join Event
                    </Button>
                  </Box>
                )}
                <Typography
                  fontSize='16px'
                  fontWeight={500}
                  color='dark'
                  textAlign='start'
                  mt='20px'
                  mb='10vh'
                >
                  {eventDetail?.description}
                </Typography>
              </Box>
            );
          }
        })}
      </Container>
      <EventFormModal eventData={eventDetail} openModal={openModal} onOpenModal={handleOpenModal} />
    </Box>
  );
};

export default EventDetail;
