import React from 'react';
import { Grid, Stack, Typography } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLocationArrow, faPhone } from '@fortawesome/free-solid-svg-icons';
import ContactForm from './../components/contact/contactForm';
import { Header } from '../components/header/header';

const ContactUs = () => {
  return (
    <>
      <Header title='Contact Us' />
      <Stack sx={{ width: '100%', height: '50vh' }}>
        <iframe
          title='Google Map'
          width='100%'
          height='100%'
          // frameBorder="0"
          style={{ border: 0 }}
          src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15282225.79979123!2d73.7250245393691!3d20.750301298393563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30635ff06b92b791%3A0xd78c4fa1854213a6!2sIndia!5e0!3m2!1sen!2sin!4v1587818542745!5m2!1sen!2sin'
          allowFullScreen
        ></iframe>
      </Stack>

      <Grid container spacing={2} marginY={4} paddingX={14}>
        <Grid item xs={6} style={{ alignItems: 'left', textAlign: 'left' }} padding={2}>
          <Typography variant='h5' fontSize='2em' marginY={4} paddingTop={6} color='#204D67'>
            GET IN TOUCH
          </Typography>
          <Typography variant='p' color='#485D88'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget
            risus sollicitudin pellentesque et non erat.
          </Typography>
          <Stack direction={'row'} spacing={4} width={'100%'} marginTop={6}>
            <Typography variant='div' width='50%'>
              <FontAwesomeIcon color='#4d49c1' icon={faLocationArrow} fontSize='3.5rem' />
              <Typography variant='h5' fontSize='1.8em' marginY={3} color='#204D67'>
                LOCATION
              </Typography>
              <Typography variant='p' color='#485D88' lineHeight={2} fontSize={15}>
                121 King Street, Melbourne Victoria 3000 United States of America, CA 90017
              </Typography>
            </Typography>
            <Typography variant='div' width='50%'>
              <FontAwesomeIcon color='#4d49c1' icon={faPhone} fontSize='3rem' />
              <Typography variant='h5' fontSize='1.8em' marginY={3} color='#204D67'>
                CALL US
              </Typography>
              <Typography variant='p' color='#485D88' lineHeight={2}>
                <Typography variant='span' fontWeight={800} fontSize={15}>
                  Phone:{' '}
                </Typography>{' '}
                (1005) 5999 4446
              </Typography>
              <br />
              <Typography variant='p' color='#485D88' lineHeight={2}>
                <Typography variant='span' fontWeight={800} fontSize={15}>
                  Email:{' '}
                </Typography>{' '}
                support@templaza.com
              </Typography>
            </Typography>
          </Stack>
        </Grid>
        <Grid item xs={6}>
          <ContactForm />
        </Grid>
      </Grid>
    </>
  );
};

export default ContactUs;
