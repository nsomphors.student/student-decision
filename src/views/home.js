import React from 'react';
import Slider from '../components/header/slider';
import FlippingCard from '../components/home/flipCard';
import UniversityReasoning from '../components/home/universityReason';
import Advisor from '../components/home/advisor';

export default function Home() {
  return (
    <>
      <Slider />
      <FlippingCard />
      <UniversityReasoning />
      <Advisor />
    </>
  );
}
