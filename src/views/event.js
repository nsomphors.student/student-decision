/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { Typography, Container, Grid, Box, Divider } from '@mui/material';
import { Header } from '../components/header/header';
import EventCard from '../components/events/eventCard';
import { eventDescription } from '../constant/messages';
import { useNavigate } from 'react-router-dom';
import { client } from '../utils/axiosEvent';

const EventPage = () => {
  const navigate = useNavigate();
  const [events, setEvents] = useState([]);

  React.useEffect(() => {
    client.get('/events').then((response) => {
      setEvents(response?.data?.event);
    });
  }, []);

  const handleClickEventCard = React.useCallback((event) => {
    console.log('press event::', event);
    navigate('/event/event-detail', { state: event });
  }, []);

  const renderEventCards = React.useMemo(() => {
    if(!events){
      return <></>
    }
    return events?.map((item) => (
      <Grid item xs={12} sm={12} md={12} lg={12} key={item?.id}>
        <Box width='100%' display='flex' flexDirection='row'>
          <EventCard event={item} onClick={handleClickEventCard} />
          <Box width='50%' px='32px'>
            <Typography
              fontSize={{
                sm: '16px',
                md: '18px',
                lg: '18px',
              }}
              fontWeight={500}
              fontFamily='revert-layer'
              alignSelf='center'
              textAlign='start'
            >
              {eventDescription.firstDescription}
            </Typography>
            <Box>
              <Typography
                fontSize={{
                  sm: '16px',
                  md: '18px',
                  lg: '18px',
                }}
                fontWeight={500}
                fontFamily='revert-layer'
                alignSelf='center'
                textAlign='start'
              >
                {eventDescription.firstDescription}
                <br /> <br />
                {eventDescription.secondDescription}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Grid>
    ));
  }, [handleClickEventCard, events]);

  return (
    <Box bgcolor='#f5f5f5'>
      <Header title='Event' />

      <Container maxWidth='80%' sx={{ background: '#f5f5f5' }}>
        <Typography
          maxWidth='lg'
          sx={{
            display: 'flex',
            alignItems: 'center',
            color: '#87a2d5',
            paddingY: 4,
            marginLeft: 6,
          }}
        >
          <Divider
            sx={{
              width: '7%',
              height: '2px',
              backgroundColor: '#87a2d5',
            }}
          />
          <Typography variant='span' color='' marginLeft={2}>
            THEY ARE ADVISORS
          </Typography>
        </Typography>
        <Typography variant='h4' fontWeight={800} color='#00256E' textAlign='left' marginLeft={6}>
          Find The Events
        </Typography>
        <Grid
          container
          spacing={2}
          justifyContent='center'
          w='100%'
          h='auto'
          py='56px'
          alignItems='center'
          px='64px'
        >
          <Grid container py='30px' spacing={12} justifyContent='center'>
            {renderEventCards}
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default EventPage;
