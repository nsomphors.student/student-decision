import axios from 'axios';

export const getAdvisorInfo = (all = true) => {
  let advisors = [];
  axios
    .get('http://localhost:3000/advisors', {
      headers: {
        'service-code': 'student_decision',
      },
    })
    .then((response) => {
      advisors = response.data;
    });
  return all ? advisors : advisors.slice(0, 4);
};
