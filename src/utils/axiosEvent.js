import axios from 'axios';

export const client = axios.create({
  //Change the base url here
  baseURL: 'http://localhost:8000',
  headers: {
    Accept: 'application/json',
    'service-code': 'student_decision',
  },
});
