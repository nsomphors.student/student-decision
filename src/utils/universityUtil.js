export function getUniversityList() {
  let universities = [];
  axios
    .get('http://localhost:3000/universities', {
      headers: {
        'service-code': 'student_decision',
      },
    })
    .then((response) => {
      universities = response.data;
    });
  return universities;
}
