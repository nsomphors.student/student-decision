# Student Decision

A website that allows students after graduating from grade 12 to find the university, events that relate to education, and meet with advisors to discuss the major or university that should they continue their study.

## Set up

1. Clone the repo<br />
   `https://gitlab.com/nsomphors.student/student-decision.git`
2. Run command `npm install` to install modules
3. Run `npm run start` or `npm start` to start project

## Eslint

1. Install dependencies
2. Check eslint error on the whole project run command `npm run lint` or `npm lint`
3. Fixed all eslint error run command `npm run lint:fix` or `npm lint:fix`
4. Format code with prettier by using command `npm run format` or `npm format`

## Home page UI

<img src="./src/images/home_page_ui.png" align="center" height="350" width="600"/>
